import uuid

from django.db import models
from django.utils import timezone


class Wallet(models.Model):
	uuid = models.UUIDField(primary_key=True, default=uuid.uuid4)

	customer_xid = models.TextField(unique=True, default='')
	token = models.CharField(max_length=40)
	balance = models.IntegerField(default=0)

	is_enabled = models.BooleanField(default=False)
	enabled_at = models.DateField(default=timezone.now)


class Deposit(models.Model):
	uuid = models.UUIDField(primary_key=True)
	wallet = models.ForeignKey(Wallet, on_delete=models.CASCADE)
	amount = models.IntegerField(default=0)
	deposited_at = models.DateField(default=timezone.now)


class Withdrawal(models.Model):
	uuid = models.UUIDField(primary_key=True)
	wallet = models.ForeignKey(Wallet, on_delete=models.CASCADE)
	amount = models.IntegerField(default=0)
	withdrawn_at = models.DateField(default=timezone.now)
