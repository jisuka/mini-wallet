Mini-Wallet
===========


A Wallet Service for a wallet feature  

More details: https://documenter.getpostman.com/view/8411283/SVfMSqA3?version=latest


Install
-------

* Run `git clone git@gitlab.com:jisuka/mini-wallet.git` to download the code
* Run `pipenv install` to install dependencies
* Run `pipenv run python manage.py migrate` to initialize Database tables

Test
----

Run `pipenv run python manage.py test` to run tests


Run locally
-----------

* Run `pipenv run python manage.py runserver` to run the service


Usage
-----

1. Create a wallet account:  
   POST: http://localhost:8000/api/v1/init
    * Requires 'customer_xid' as payload.
    * Returns a token. 
    * Pass 'Token my_token' in Authorization header with all the following requests.


2. Enable the wallet:  
   POST: http://localhost:8000/api/v1/wallet
    * If the wallet is already enabled, this fails.

3. View balance:  
  GET: http://localhost:8000/api/v1/wallet

4. Add money:  
  POST: http://localhost:8000/api/v1/wallet/deposits
   * Requires 'amount' and a unique 'reference_id' as payload.

5. Use money:  
  POST: http://localhost:8000/api/v1/wallet/withdrawals
   * Requires 'amount' and a unique 'reference_id' as payload.
   * The balance should be greater than the amount specified, or this fails.

6. Disable wallet:  
  PATCH: http://localhost:8000/api/v1/wallet
