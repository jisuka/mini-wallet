import functools

from rest_framework.response import Response
from rest_framework import status

from wallet.models import Wallet


def valid_token_required(func):
    """Perform authorization check

    1. Checks that Authorization header is present
    2. Checks that the value is in the required format 'Token <token>'
    3. Verifies that the token value corresponds to a token present in the database
    4. Passes the Wallet object to the view function

    Todo:
    This can be refactored by using a Custom TokenAuthentication Scheme:
    https://www.django-rest-framework.org/api-guide/authentication/#tokenauthentication
    """

    def wrapper(*args, **kwargs):
        request = args[0]

        if 'Authorization' not in request.headers:
            return Response({
                'status': 'fail',
                'data': {
                    'Authorization': 'Authorization header is missing'
                }}, 
                status=status.HTTP_400_BAD_REQUEST)
        
        auth_header = request.headers['Authorization']

        try:
            token = auth_header.split(' ')[1]
            w = Wallet.objects.get(token=token)
        except:
            return Response({
                'status': 'fail',
                'data': {
                    'Authorization': 'Invalid credentials'
                }}, 
                status=status.HTTP_401_UNAUTHORIZED)

        token = request.headers['Authorization'].split(' ')[1]
        w = Wallet.objects.get(token=token)

        return func(*args, w, **kwargs)
    return wrapper


def wallet_is_enabled(func):
    """ Wallet should be enabled for viewing, deposits and withdrawals """
    
    def wrapper(*args, **kwargs):
        request = args[0]
        wallet = args[1]

        if not wallet.is_enabled:
            return Response({
                'status': 'fail',
                'data': {
                    'Enable': 'Wallet is not enabled'
                }}, 
                status=status.HTTP_403_FORBIDDEN)

        return func(*args, **kwargs)
    return wrapper
