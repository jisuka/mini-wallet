import os
import requests
import binascii

from wallet.models import Wallet


def authenticate_customer(customer_xid, password=None):
    # Authenticate, obtain token 

    # By talking to Customer Service
    # url = 'http://api.example.com/authenticate_customer' 
    # params = {'customer_xid': customer_xid, 'password': password}
    # resp = requests.get(url, params=params)
    # return resp.json()


    # Fill and use a 'token' field in Wallet Model
    # to mock the Customer service
    w = Wallet.objects.filter(customer_xid=customer_xid).first()

    if w:
        token = w.token
    else:
        token = binascii.hexlify(os.urandom(20)).decode()
        Wallet.objects.create(customer_xid=customer_xid, token=token)

    return {
        'status': 'success',
        'data': {
            'token': token
        }
    }
    