from datetime import datetime

from django.shortcuts import render
from django.db.utils import IntegrityError

from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view

from wallet.models import Wallet, Deposit, Withdrawal

from api.services import authenticate_customer
from api.decorators import valid_token_required, wallet_is_enabled


@api_view(['POST'])
def wallet_init(request):
    """ Create Wallet account if not already created for the customer 

    Return token
    """

    auth_resp = authenticate_customer(
        request.POST['customer_xid'],
        # request.POST['password'], 
    )

    return Response({
        'status': 'success',
        'data': {
            'token': auth_resp['data']['token']
        }
    })


@api_view(['POST', 'GET', 'PATCH'])
@valid_token_required
def wallet(request, w):
    """ 
    GET: View balance
    POST: Enable wallet
    PATCH: Disable wallet 

    Return wallet details on success
    """

    if request.method == 'GET':
        if not w.is_enabled:
            return Response({
                'status': 'fail',
                'data': {
                    'Enable': 'Wallet is not enabled'
                }}, 
                status=status.HTTP_403_FORBIDDEN)

    elif request.method == 'POST':
        if w.is_enabled:
            return Response({
                'status': 'fail',
                'data': {
                    'Enable': 'Already enabled'
                }}, 
                status=status.HTTP_403_FORBIDDEN)
        
        w.is_enabled = True
        w.save()

    elif request.method == 'PATCH':
        w.is_enabled = False
        w.save()
        return Response({
            'status': 'success',
            'data': {
                'wallet': {
                    'id': w.uuid,
                    'owned_by': w.customer_xid,
                    'status': 'enabled' if w.is_enabled else 'disabled',
                    'disabled_at': datetime.now(),
                    'balance': w.balance,
                },
            }
        }) 

    return Response({
        'status': 'success',
        'data': {
            'wallet': {
                'id': w.uuid,
                'owned_by': w.customer_xid,
                'status': 'enabled' if w.is_enabled else 'disabled',
                'enabled_at': w.enabled_at,
                'balance': w.balance,
            },
        }
    }) 


@api_view(['POST'])
@valid_token_required
@wallet_is_enabled
def deposit(request, w):
    """ 
    POST: Add money to wallet

    Authorization Token is required
    Wallet should be enabled
    Reference ID should be unique

    Return transaction details on success
    """

    amount = int(request.POST['amount'])

    try:
        d = Deposit.objects.create(
            uuid = request.POST['reference_id'],
            wallet=w,
            amount=amount,
        )
    except IntegrityError:
        return Response({
            'status': 'fail',
            'data': {
                'Enable': 'Reference id already exists'
            }}, 
            status=status.HTTP_403_FORBIDDEN)

    w.balance += amount
    w.save()

    return Response({
        'status': 'success',
        'data': {
            'deposit': {
                'id': w.uuid,
                'deposited_by': w.customer_xid,
                'status': 'success',
                'deposited_at': d.deposited_at,
                'amount': amount,
                'reference_id': d.uuid, 
            },
        }
    }) 


@api_view(['POST'])
@valid_token_required
@wallet_is_enabled
def withdraw(request, w):
    """ 
    POST: withdraw money to wallet

    Authorization Token required
    Wallet should be enabled
    Reference ID should be unique
    Balance should be sufficient

    Return transaction details on success
    """

    amount = int(request.POST['amount'])

    try:
        wd = Withdrawal.objects.create(
            uuid = request.POST['reference_id'],
            wallet=w,
            amount=amount,
        )
    except IntegrityError:
        return Response({
            'status': 'fail',
            'data': {
                'Enable': 'Reference id already exists'
            }}, 
            status=status.HTTP_403_FORBIDDEN)

    if amount > w.balance:
        return Response({
            'status': 'fail',
            'data': {
                'Amount': 'Insufficient balance'
            }}, 
            status=status.HTTP_403_FORBIDDEN)

    w.balance -= amount
    w.save()

    return Response({
        'status': 'success',
        'data': {
            'withdrawal': {
                'id': w.uuid,
                'withdrawn_by': w.customer_xid,
                'status': 'success',
                'withdrawn_at': wd.withdrawn_at,
                'amount': amount,
                'reference_id': wd.uuid, 
            },
        }
    }) 
